package com.example.connect4;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Class represents main class of project. It creates main screen.
 */
public class MainActivity extends Activity {
    /**
     * Media player for main screen music
     */
    private MediaPlayer player;
    /**
     * Buttons for navigating to other activities
     */
    Button playvscomp, playvsplayer, options, stats;

    /**
     * Initiating all buttons for navigating through project.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playvscomp = (Button)findViewById(R.id.playvscomp);
        playvsplayer = (Button)findViewById(R.id.playvsplayer);
        options = (Button)findViewById(R.id.options);
        stats = (Button)findViewById(R.id.stat);


        playvscomp.setOnClickListener(new PlayvsCompListener());

        playvsplayer.setOnClickListener(new PlayvsPlayerListener());

        options.setOnClickListener(new OptionListener());

        stats.setOnClickListener(new StatsListener());

        player = MediaPlayer.create(this, R.raw.menu);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        // checking if music is allowed by user in options.
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1){
            player.setLooping(true);
            player.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player.isPlaying()){
            player.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if(!player.isPlaying() && sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1) {
            player.setLooping(true);
            player.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player.isPlaying()){
            player.pause();
        }
    }

    /**
     * Listener Class for playing vs computer
     */
    private class PlayvsCompListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            startActivity(new Intent(MainActivity.this, GameBoardDroid.class));
        }
    }

    /**
     * Listener Class for playing vs player
     */
    private class PlayvsPlayerListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            startActivity(new Intent(MainActivity.this, GameBoardPlayer.class));
        }
    }

    /**
     * Listener Class for getting into option screen
     */
    private class OptionListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            startActivity(new Intent(MainActivity.this,GameOptions.class));
        }
    }

    /**
     * Listener Class for getting into statistic screen
     */
    private class StatsListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
            startActivity(new Intent(MainActivity.this,GameStatistics.class));
        }
    }



}
