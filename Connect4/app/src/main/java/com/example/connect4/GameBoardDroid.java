package com.example.connect4;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class representing Activity with game vs computer.
 */
public class GameBoardDroid extends AppCompatActivity {
    /**
     * Media player for game screen music.
     */
    private MediaPlayer player;
    /**
     * Handler used to delay computer move.
     */
    private Handler myhandler;
    /**
     * Media player for player move sound.
     */
    private MediaPlayer playermoveplayer;
    /**
     * Media player for droid move sound.
     */
    private MediaPlayer droidmoveplayer;
    /**
     * Int array representing game board.
     */
    int gameBoard[][] = new int[6][7];

    /**
     * Boolean indicating if its players turn.
     */
    static boolean playerTurn;

    /**
     * Method initiates gameboard buttons and background.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        playerTurn = true;

        //initializing the gameBoard
        for (int i = 0 ; i < 6 ; i++) {Arrays.fill(gameBoard[i], 0);}

        // applying the listener to all the columns and the buttons inside of them
        // so when player touches any column right button is colored.
        ViewGroup cols =(ViewGroup) findViewById(R.id.wrapper);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String plays = sp.getString(MyConstants.GameBoard.DROIDBOARD, "");
        //loop over the 6 columns
        for(int i = 0, k = 0; i < 6 ;k++){

            View child = cols.getChildAt(k);
            

            if (!(child instanceof ViewGroup)){continue;}
            ViewGroup col = (ViewGroup) child;
            
            //add the listener to the column itself
            col.setOnClickListener(new PlayListener(i));
            

            for(int j = 0 ; j < col.getChildCount() ; j++){
                col.getChildAt(j).setOnClickListener(new PlayListener(i));
            }

            i++;
        }

        // checks if there were any moves already played previously. So
        // you can continue in previous game.
        if(!(plays.equals(""))) {
            String[] moves = plays.split(";");
            for (int h = 0; h < moves.length; h++) {
                String[] move = moves[h].split(",");
                int player = Integer.parseInt(move[0]);
                int col = Integer.parseInt(move[1]);
                loadMove(player, col, GameBoardDroid.this.gameBoard);
            }
        }

        //add the listener for the restart button
        findViewById(R.id.restart).setOnClickListener(new RestartListener());
        player = MediaPlayer.create(this, R.raw.game);
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1){
            player.setLooping(true);
            player.start();
        }
        playermoveplayer = MediaPlayer.create(this, R.raw.highclick);
        droidmoveplayer = MediaPlayer.create(this, R.raw.shortclock);
        myhandler = new Handler();
    }

    /**
     * Method saves move into game board and colors appropriate button.
     * @param player - number of player who made a move.
     * @param col - number of column in which move was made.
     * @param board - game board.
     */
    private void play(int player, int col, int board[][]){
        int colId = getColId(col);
        int row = getFirstAvailableRow(col);

        //changes the circles color according to the player and mark the array
        if(player == MyConstants.GameBoard.PLAYER){
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = sp.edit();
            // appends new move with older moves.
            String plays = sp.getString(MyConstants.GameBoard.DROIDBOARD, "");
            plays += player+","+col+";";
            editor.putString(MyConstants.GameBoard.DROIDBOARD, plays);
            // checks if game sounds are allowed and if so, plays blue player sound.
            if(sp.getInt(MyConstants.GameOptions.SOUNDS, 1) == 1){
                playermoveplayer.start();
            }
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_blue));
            board[col][row] = MyConstants.GameBoard.PLAYER;
            editor.apply();
        }
        else if(player == MyConstants.GameBoard.DROID){
            myhandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, 600);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = sp.edit();
            // appends new move with older moves.
            String plays = sp.getString(MyConstants.GameBoard.DROIDBOARD, "");
            plays += player+","+col+";";
            editor.putString(MyConstants.GameBoard.DROIDBOARD, plays);
            // checks if game sounds are allowed and if so, plays blue player sound.
            if(sp.getInt(MyConstants.GameOptions.SOUNDS, 1) == 1){
                droidmoveplayer.start();
            }
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_red));
            board[col][row] = MyConstants.GameBoard.DROID;
            editor.apply();
        }

    }

    /**
     * Method for resuming game board into previous state.
     * @param player - number of player who made a move.
     * @param col - number of column in which move was made.
     * @param board - game board.
     */
    private void loadMove(int player, int col, int board[][]){
        int colId = getColId(col);
        int row = getFirstAvailableRow(col);
        //changes the circles color according to the player and mark the array
        if(player == MyConstants.GameBoard.PLAYER){
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_blue));
            board[col][row] = MyConstants.GameBoard.PLAYER;
        }
        else if(player == MyConstants.GameBoard.DROID){
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_red));
            board[col][row] = MyConstants.GameBoard.DROID;
        }
    }

    /**
     * Method searches for first available row in certain column
     * @param col - number of column in which we need to find available row.
     */
    private int getFirstAvailableRow(int col){
        for(int i = 0 ; i < 7 ; i++){
            if(gameBoard[col][i] == 0){
                return i;
            }
        }
        return -1;
    }

    /**
     * Method returns Column Id
     */
    private int getColId(int col){

        switch(col){
            case 0:
                return R.id.col1;
            case 1:
                return R.id.col2;
            case 2:
                return R.id.col3;
            case 3:
                return R.id.col4;
            case 4:
                return R.id.col5;
            case 5:
                return R.id.col6;
            default:
                return -1;
        }
    }
	
	/**
     * Method searches the game board. If someone won or if the game is tied. 0 if noone won yet.
     * @param b - representation of game board
     */
    private int getWinner(int b[][]){

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 0 ; y < 7-3 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y+1] && b[x+1][y+1] == b[x+2][y+2] && b[x+2][y+2] == b[x+3][y+3]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 3 ; y < 7 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y-1] && b[x+1][y-1] == b[x+2][y-2] && b[x+2][y-2] == b[x+3][y-3]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 0 ; y < 7 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y] && b[x+1][y] == b[x+2][y] && b[x+2][y] == b[x+3][y]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6 ; x++){
            for(int y = 0 ; y < 7-3 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x][y+1] && b[x][y+1] == b[x][y+2] && b[x][y+2] == b[x][y+3]){
                    return b[x][y];
                }
            }
        }

        boolean tie = true;
        for(int i = 0 ; i < 6 ; i++){
            tie = (getFirstAvailableRow(i) == -1) && tie;
        }

        if(tie) return -1;

        return 0;
    }

    /**
     * Method anounces winner.
     * @param b - board of the game.
     * @return - return true, indicates that the game is done
     */
    private boolean announceWinner(int b[][]){
        int state = getWinner(b);
        if (state == 0) return false;
        //to disable player's interaction with the board
        playerTurn = false;
	    String plays = "";
		//show restart button
        findViewById(R.id.restart).setVisibility(View.VISIBLE);
        TextView textView = (TextView) findViewById(R.id.state);
        textView.setVisibility(View.VISIBLE);	//make it visible
		
        //resets moves played this game
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(MyConstants.GameBoard.DROIDBOARD, plays);
        int old;

        if(state == MyConstants.GameBoard.PLAYER){
			

            textView.setText(R.string.won);
			
			//get the current difficulty and increment it
            switch (sp.getInt(MyConstants.GameOptions.DIFFICULTY, MyConstants.GameOptions.MODE_EASY)){
                case MyConstants.GameOptions.MODE_EASY:
                    old = sp.getInt(MyConstants.Statistics.PREF_EASY_WON, 0);
                    editor.putInt(MyConstants.Statistics.PREF_EASY_WON, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_MEDIUM:
                    old = sp.getInt(MyConstants.Statistics.PREF_MED_WON, 0);
                    editor.putInt(MyConstants.Statistics.PREF_MED_WON, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_HARD:
                    old = sp.getInt(MyConstants.Statistics.PREF_HARD_WON, 0);
                    editor.putInt(MyConstants.Statistics.PREF_HARD_WON, old + 1);
                    break;
                default:

                    Toast.makeText(this, "Error while submitting results - Easy", Toast.LENGTH_SHORT).show();
            }
        }
        else if (state == MyConstants.GameBoard.DROID){

            textView.setText(R.string.lost);

            switch (sp.getInt(MyConstants.GameOptions.DIFFICULTY, MyConstants.GameOptions.MODE_EASY)){
                case MyConstants.GameOptions.MODE_EASY:
                    old = sp.getInt(MyConstants.Statistics.PREF_EASY_LOST, 0);
                    editor.putInt(MyConstants.Statistics.PREF_EASY_LOST, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_MEDIUM:
                    old = sp.getInt(MyConstants.Statistics.PREF_MED_LOST, 0);
                    editor.putInt(MyConstants.Statistics.PREF_MED_LOST, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_HARD:
                    old = sp.getInt(MyConstants.Statistics.PREF_HARD_LOST, 0);
                    editor.putInt(MyConstants.Statistics.PREF_HARD_LOST, old + 1);
                    break;
                default:
                    Toast.makeText(this, "Error while submitting results - Medium", Toast.LENGTH_SHORT).show();
            }
        }
        else {

            textView.setText(R.string.tie);

            switch (sp.getInt(MyConstants.GameOptions.DIFFICULTY, MyConstants.GameOptions.MODE_EASY)){
                case MyConstants.GameOptions.MODE_EASY:
                    old = sp.getInt(MyConstants.Statistics.PREF_EASY_TIE, 0);
                    editor.putInt(MyConstants.Statistics.PREF_EASY_TIE, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_MEDIUM:
                    old = sp.getInt(MyConstants.Statistics.PREF_MED_TIE, 0);
                    editor.putInt(MyConstants.Statistics.PREF_MED_TIE, old + 1);
                    break;
                case MyConstants.GameOptions.MODE_HARD:
                    old = sp.getInt(MyConstants.Statistics.PREF_HARD_TIE, 0);
                    editor.putInt(MyConstants.Statistics.PREF_HARD_TIE, old + 1);
                    break;
                default:
                    Toast.makeText(this, "Error while submitting results - Hard", Toast.LENGTH_SHORT).show();
            }

        }
		
		//apply changes to the shared prefrences
        editor.apply();

        return true;
    }


    /**
     * Class listening to columns played by player.
     */
    private class PlayListener implements View.OnClickListener{

        int player, col, colId;
		
		//take column's numberr to play in that column when invoked
        public PlayListener(int c){
            col = c;
            player = MyConstants.GameBoard.PLAYER;
            colId = getColId(c);
        }

        @Override
        public void onClick(View v){
			//if the column is full or not the player's turn yet, do nothing
            if(getFirstAvailableRow(col) == -1 || !GameBoardDroid.playerTurn) return;

            play(player, col, GameBoardDroid.this.gameBoard);

            if(announceWinner(GameBoardDroid.this.gameBoard)) return;
            
            //Player's turn is done
            GameBoardDroid.playerTurn = false;
            
            //launch program's thinking thread
            new DroidThink().execute();

        }
    }


    /**
     * Class listening to restart button
     */
    private class RestartListener implements View.OnClickListener{
        @Override
        public void onClick(View v){
			
			//resets all the circles
            ViewGroup cols =(ViewGroup) findViewById(R.id.wrapper);
            for(int i = 0, k = 0; i < 6 ;k++){
                View child = cols.getChildAt(k);
                if (!(child instanceof ViewGroup)){continue;}
                ViewGroup col = (ViewGroup) child;
                for(int j = 0 ; j < col.getChildCount() ; j++){
                    col.getChildAt(j).setBackground(getResources().getDrawable(R.drawable.circle_white));
                }
                i++;
            }
			
			//resets the board-array
            for (int i = 0 ; i < 6 ; i++) {Arrays.fill(gameBoard[i], 0);}

			//hides it self and the state's textView
            findViewById(R.id.state).setVisibility(View.INVISIBLE);
            v.setVisibility(View.INVISIBLE);
            
            //player's turn to play
            playerTurn = true;
        }
    }

    /**
     * Class representing Droid thinking with minmax algorythm.
     */
    private class DroidThink extends AsyncTask <Void, Void, Integer>{

        /**
         * max depth for the minimax recursion
         */
        int maxDepth;

        private int minimax_rec(int player,int cDepth){

            if(isTerminal(gameBoard) || cDepth == maxDepth){
                return utility(cDepth);
            }

            int min = 1000000, max = -1000000, m;
            int cmax = 0, cmin = 0, c = 0;

            for(int col = 0 ; col < 6 ; col++) {
                int row = getFirstAvailableRow(col);
                if (row == -1) continue;
                gameBoard[col][row] = player;
                m = minimax_rec(player ^ 3, cDepth + 1);
                c++;
                gameBoard[col][row] = 0;

                if (m == max && max > 0) {
                    cmax++;
                } else {
                    cmax = 0;
                }
                if (m == min && min < 0) {
                    cmin++;
                } else {
                    cmin = 0;
                }

                max = Math.max(m, max);
                min = Math.min(m, min);

            }

            if(maxDepth > MyConstants.GameOptions.MODE_EASY){
                //if any play from here leads to winning,
                // give greater maximum value (force playing here)
                if(c == cmax){
                    max = 200;
                }
                //if any play from here leads to losing,
                // give very low -ve value (force playing away)
                if(c == cmin){
                    min = -200;
                }
            }

            if(player == MyConstants.GameBoard.PLAYER){
                return min;
            }else{
                return max;
            }
        }

        /**
         * loops over the columns and gets the final expected value of each column
         */
        private int minimax(){
            List<Integer> list = new ArrayList<>();
            int max = -1000000, m;
            int row;
            int d = MyConstants.GameBoard.DROID;
            for(int col = 0 ; col < 6 ; col++){
                row = getFirstAvailableRow(col);
                if(row == -1) continue;
                gameBoard[col][row] = d;
                m = minimax_rec(MyConstants.GameBoard.PLAYER, 0);
                gameBoard[col][row] = 0;
                if(m > max || max == -1000000){
                    max = m;
                    list.clear();
                    list.add(col);
                }else if (m ==  max){
                    list.add(col);
                }
            }

            //pick a random column from the list (to make the behavior unpredictable)
            return list.get(new Random().nextInt(list.size()));
        }

        /**
         * cheks the board and tells if the game is done or not
         * @param b - game board representation
         */
        private boolean isTerminal(int b[][]){
            return (getWinner(b) != 0);
        }
		
		
		/**
         * get the value of the current board state (the testing board this thread has, not the real board)
         * +ve number ==> good for the Computer (Computer wins), -ve number ==> bad for the Computer (computer loses)
         * 0 ==> game not done or tie (nuetral state)
         * @param cd - current depth
         */
        private int utility(int cd){
            //get the state
            int state = getWinner(gameBoard);
            //if computer loses returns -ve number that is lower as the computer loses sooner
            if(state == MyConstants.GameBoard.PLAYER){
                return -maxDepth - 1 + cd;
            
            //if the computer wins, returns +ve number that is greater as the computer wins sooner
            }else if(state == MyConstants.GameBoard.DROID){
                return maxDepth + 1 - cd;
            }
            
            //in case no one wins or tie
            return 0;
        }

        @Override
        protected Integer doInBackground(Void ... params){
        	//the value of the difficulty constant determines the maxDepth of the recursion
            maxDepth = PreferenceManager.getDefaultSharedPreferences(getBaseContext())
            							.getInt(MyConstants.GameOptions.DIFFICULTY, MyConstants.GameOptions.MODE_EASY);

            //return the value of the minimax
            return minimax();
        }

        @Override
        protected void onPostExecute(Integer col){
        	//plays in the place that the program decided
            play(MyConstants.GameBoard.DROID, col,  GameBoardDroid.this.gameBoard);
            
            //if the game is done (Computer won or tie) return, no need to give the player access to the board
            if (announceWinner(GameBoardDroid.this.gameBoard)) return;
            
            //player's turn, the listener will now play in the place he chooses
            GameBoardDroid.playerTurn = true;
        }

    }

    /**
     * Method listens to Back button.
     */
    public void onBackClick(View view){
        if (view.getId() == R.id.goback) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player.isPlaying()){
            player.pause();
            playermoveplayer.pause();
            droidmoveplayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if(!player.isPlaying() && sp.getInt(MyConstants.GameOptions.MUSIC, 1) ==  1) {
            player.setLooping(true);
            player.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player.isPlaying()){
            player.pause();
            playermoveplayer.pause();
            droidmoveplayer.pause();
        }
    }


}
