package com.example.connect4;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;



public class GameOptions extends AppCompatActivity {
    /**
     * Media player for playing option screen music.
     */
    private MediaPlayer player;
    /**
     * Shared Preferences used for saving important data.
     */
    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    /**
     * Radio Buttons for selecting computer difficulty
     */
    private RadioButton easy, med, hard;
    /**
     * CheckBoxes for disabling and enabling music and sounds.
     */
    private CheckBox music, sounds;

    /**
     * Initiating all TextViews, Buttons and CheckBoxes of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_options);

        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        spEditor = sp.edit();

        easy = (RadioButton)findViewById(R.id.setEasy);
        med = (RadioButton)findViewById(R.id.setMed);
        hard = (RadioButton)findViewById(R.id.setHard);

        music = (CheckBox)findViewById(R.id.music);
        sounds = (CheckBox)findViewById(R.id.sounds);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        // determines state of checkboxes based on previous setup.
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1){
            music.setChecked(false);
        }
        else{
            music.setChecked(true);
        }
        if(sp.getInt(MyConstants.GameOptions.SOUNDS, 1) == 1){
            sounds.setChecked(false);
        }
        else{
            sounds.setChecked(true);
        }

        setState(sp.getInt(MyConstants.GameOptions.DIFFICULTY, MyConstants.GameOptions.MODE_EASY));

        easy.setOnClickListener(new SetMode(MyConstants.GameOptions.MODE_EASY));
        med.setOnClickListener(new SetMode(MyConstants.GameOptions.MODE_MEDIUM));
        hard.setOnClickListener(new SetMode(MyConstants.GameOptions.MODE_HARD));

        findViewById(R.id.resetStat).setOnClickListener(new ResetStat());
        player = MediaPlayer.create(this, R.raw.statsoptins);

        // checks if music is allowed by user in options
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1) {
            player.setLooping(true);
            player.start();
        }

    }


    /**
     * Disables or enables music in whole project
     */
    public void onMusicClick(View view) {
        // Is the view checked?
        boolean checked = ((CheckBox) view).isChecked();

        if (view.getId() == R.id.music) {
            if (checked) {
                spEditor.putInt(MyConstants.GameOptions.MUSIC, 0);
                player.pause();

            } else {
                spEditor.putInt(MyConstants.GameOptions.MUSIC, 1);
                player.setLooping(true);
                player.start();
            }
        }
        spEditor.apply();
    }

    /**
     * Disables or enables game sounds in whole project
     */
    public void onSoundsClick(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        if (view.getId() == R.id.sounds) {
            if (checked) {
                spEditor.putInt(MyConstants.GameOptions.SOUNDS, 0);
            } else {
                spEditor.putInt(MyConstants.GameOptions.SOUNDS, 1);
            }
        }
        spEditor.apply();
    }

    /**
     * Method listens to Back button.
     */
    public void onBackClick(View view){
        if (view.getId() == R.id.backbutton) {
            finish();
        }
    }

    /**
     * Method resets all statistics saved until this point.
     */
    private class ResetStat implements View.OnClickListener{
        @Override
        public void onClick(View v){
            spEditor.putInt(MyConstants.Statistics.PREF_EASY_WON, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_EASY_LOST, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_EASY_TIE, 0);

            spEditor.putInt(MyConstants.Statistics.PREF_MED_WON, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_MED_LOST, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_MED_TIE, 0);

            spEditor.putInt(MyConstants.Statistics.PREF_HARD_WON, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_HARD_LOST, 0);
            spEditor.putInt(MyConstants.Statistics.PREF_HARD_TIE, 0);

            spEditor.putInt(MyConstants.GameOptions.SOUNDS, 1);
            spEditor.putInt(MyConstants.GameOptions.MUSIC, 1);

            spEditor.apply();

            Toast.makeText(getBaseContext(), "Statistics Reset", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Sets difficulty for computer.
     */
    private class SetMode implements View.OnClickListener{
        private final int mode;

        public SetMode(int m){
            mode = m;
        }
        @Override
        public void onClick(View v){
            spEditor.putInt(MyConstants.GameOptions.DIFFICULTY, mode);
            spEditor.apply();
        }
    }

    /**
     * Sets and initializes difficulty for computer.
     */
    private void setState(int m){

        //initialize the 3 radio buttons
        easy.setChecked(false);
        med.setChecked(false);
        hard.setChecked(false);

        switch(m){
            case MyConstants.GameOptions.MODE_EASY:
                easy.setChecked(true);
            break;
            case MyConstants.GameOptions.MODE_MEDIUM:
                med.setChecked(true);
            break;
            case MyConstants.GameOptions.MODE_HARD:
                hard.setChecked(true);
            break;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player.isPlaying()){
            player.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if(!player.isPlaying() && sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1) {
            player.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player.isPlaying()){
            player.pause();
        }
    }

}
