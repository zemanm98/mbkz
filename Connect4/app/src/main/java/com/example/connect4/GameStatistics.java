package com.example.connect4;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

/**
 * Class representing Statistic Activity. Creates screen with statistics.
 */
public class GameStatistics extends AppCompatActivity {

    /**
     * Media player for music in statistics screen.
     */
    private MediaPlayer player;

    /**
     * Initiating all TextViews with games won, lost and tied for each difficulty.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_statistics);

        TextView easyWon = (TextView)findViewById(R.id.easyWon);
        TextView easyLost = (TextView)findViewById(R.id.easyLost);
        TextView easyTie = (TextView)findViewById(R.id.easyTie);

        TextView medWon = (TextView)findViewById(R.id.medWon);
        TextView medLost = (TextView)findViewById(R.id.medLost);
        TextView medTie = (TextView)findViewById(R.id.medTie);

        TextView hardWon = (TextView)findViewById(R.id.hardWon);
        TextView hardLost = (TextView)findViewById(R.id.hardLost);
        TextView hardTie = (TextView)findViewById(R.id.hardTie);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());


        easyWon.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_EASY_WON, 0)));
        easyLost.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_EASY_LOST, 0)));
        easyTie.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_EASY_TIE, 0)));

        medWon.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_MED_WON, 0)));
        medLost.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_MED_LOST, 0)));
        medTie.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_MED_TIE, 0)));

        hardWon.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_HARD_WON, 0)));
        hardLost.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_HARD_LOST, 0)));
        hardTie.setText(String.valueOf(sp.getInt(MyConstants.Statistics.PREF_HARD_TIE, 0)));
        player = MediaPlayer.create(this, R.raw.statsoptins);

        // checks if music is allowed by player in options.
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1){
            player.setLooping(true);
            player.start();
        }

    }

    /**
     * Method listens to Back button.
     */
    public void onBackClick(View view){
        if (view.getId() == R.id.back) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player.isPlaying()){
            player.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if(!player.isPlaying() && sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1) {
            player.setLooping(true);
            player.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player.isPlaying()){
            player.pause();
        }
    }
}
