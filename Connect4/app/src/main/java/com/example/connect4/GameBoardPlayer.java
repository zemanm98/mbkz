package com.example.connect4;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

/**
 * Class representing Activity with game vs another player.
 */
public class GameBoardPlayer extends AppCompatActivity {

    /**
     * Media player for game screen music.
     */
    private MediaPlayer player;
    /**
     * Media player for blue player (first) move sound.
     */
    private MediaPlayer playermoveplayer;
    /**
     * Media player for red player (second) move sound.
     */
    private MediaPlayer secondmoveplayer;
    /**
     * Int array representing game board.
     */
    int gameBoard[][] = new int[6][7];
    /**
     * Boolean indicating which player is on move.
     */
    boolean blueplaying;
    /**
     * Boolean indicating if game has ended so no one can make any more moves.
     */
    boolean gamedone = false;

    /**
     * Method initiates gameboard buttons and background.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String plays = sp.getString(MyConstants.GameBoard.PLAYERBOARD, "");
        // initializing the gameBoard
        for (int i = 0 ; i < 6 ; i++) {Arrays.fill(gameBoard[i], 0);}

        // applying the listener to all the columns and the buttons inside of them
        // so when player touches any column right button is colored.
        ViewGroup cols =(ViewGroup) findViewById(R.id.wrapper);

        for(int i = 0, k = 0; i < 6 ;k++){

            View child = cols.getChildAt(k);


            if (!(child instanceof ViewGroup)){continue;}
            ViewGroup col = (ViewGroup) child;

            // add the listener to the column itself
            col.setOnClickListener(new PlayListener(i));

            for(int j = 0 ; j < col.getChildCount() ; j++){
                col.getChildAt(j).setOnClickListener(new PlayListener(i));
            }

            i++;
        }

        // checks if there were any moves already played previously. So
        // you can continue in previous game.
        if(!(plays.equals(""))) {
            String[] moves = plays.split(";");
            for (int h = 0; h < moves.length; h++) {
                String[] move = moves[h].split(",");
                int player = Integer.parseInt(move[0]);
                int col = Integer.parseInt(move[1]);
                loadMove(player, col, GameBoardPlayer.this.gameBoard);
            }
        }

        // add the listener for the restart button
        findViewById(R.id.restart).setOnClickListener(new RestartListener());
        player = MediaPlayer.create(this, R.raw.game);
        if(sp.getInt(MyConstants.GameOptions.MUSIC, 1) == 1){
            player.setLooping(true);
            player.start();
        }
        playermoveplayer = MediaPlayer.create(this, R.raw.highclick);
        secondmoveplayer = MediaPlayer.create(this, R.raw.shortclock);
        blueplaying = true;
    }

    /**
     * Method saves move into game board and colors appropriate button.
     * @param player - number of player who made a move.
     * @param col - number of column in which move was made.
     * @param board - game board.
     */
    private void play(int player, int col, int board[][]){

        int colId = getColId(col);
        int row = getFirstAvailableRow(col);

        //changes the circles color according to the player and mark the array
        if(player == MyConstants.GameBoard.PLAYER){
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = sp.edit();
            // appends new move with older moves.
            String plays = sp.getString(MyConstants.GameBoard.PLAYERBOARD, "");
            plays += player+","+col+";";
            editor.putString(MyConstants.GameBoard.PLAYERBOARD, plays);
            // checks if game sounds are allowed and if so, plays blue player sound.
            if(sp.getInt(MyConstants.GameOptions.SOUNDS, 1) == 1){
                playermoveplayer.start();
            }
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_blue));
            board[col][row] = MyConstants.GameBoard.PLAYER;
            editor.apply();
        }
        else if(player == MyConstants.GameBoard.DROID){
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = sp.edit();
            // appends new move with older moves.
            String plays = sp.getString(MyConstants.GameBoard.PLAYERBOARD, "");
            plays += player+","+col+";";
            editor.putString(MyConstants.GameBoard.PLAYERBOARD, plays);
            // checks if game sounds are allowed and if so, plays blue player sound.
            if(sp.getInt(MyConstants.GameOptions.SOUNDS, 1) == 1){
                secondmoveplayer.start();
            }
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_red));
            board[col][row] = MyConstants.GameBoard.DROID;
            editor.apply();
        }
    }

    /**
     * Method for resuming game board into previous state.
     * @param player - number of player who made a move.
     * @param col - number of column in which move was made.
     * @param board - game board.
     */
    private void loadMove(int player, int col, int board[][]){
        int colId = getColId(col);
        int row = getFirstAvailableRow(col);
        //changes the circles color according to the player and mark the array
        if(player == MyConstants.GameBoard.PLAYER){
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_blue));
            board[col][row] = MyConstants.GameBoard.PLAYER;
        }
        else if(player == MyConstants.GameBoard.DROID){
            ((ViewGroup)findViewById(colId)).getChildAt(6-row).setBackground(getResources().getDrawable(R.drawable.circle_red));
            board[col][row] = MyConstants.GameBoard.DROID;
        }
    }

    /**
     * Method searches for first available row in certain column
     * @param col - number of column in which we need to find available row.
     */
    private int getFirstAvailableRow(int col){
        for(int i = 0 ; i < 7 ; i++){
            if(gameBoard[col][i] == 0){
                return i;
            }
        }
        return -1;
    }


    /**
     * Method returns Column Id
     */
    private int getColId(int col){

        switch(col){
            case 0:
                return R.id.col1;
            case 1:
                return R.id.col2;
            case 2:
                return R.id.col3;
            case 3:
                return R.id.col4;
            case 4:
                return R.id.col5;
            case 5:
                return R.id.col6;
            default:
                return -1;
        }
    }

    /**
     * Method searches the game board. If someone won or if the game is tied. 0 if noone won yet.
     * @param b - representation of game board
     */
    private int getWinner(int b[][]){

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 0 ; y < 7-3 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y+1] && b[x+1][y+1] == b[x+2][y+2] && b[x+2][y+2] == b[x+3][y+3]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 3 ; y < 7 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y-1] && b[x+1][y-1] == b[x+2][y-2] && b[x+2][y-2] == b[x+3][y-3]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6-3 ; x++){
            for(int y = 0 ; y < 7 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x+1][y] && b[x+1][y] == b[x+2][y] && b[x+2][y] == b[x+3][y]){
                    return b[x][y];
                }
            }
        }

        for(int x = 0 ; x < 6 ; x++){
            for(int y = 0 ; y < 7-3 ; y++){
                if(b[x][y] != 0 && b[x][y] == b[x][y+1] && b[x][y+1] == b[x][y+2] && b[x][y+2] == b[x][y+3]){
                    return b[x][y];
                }
            }
        }

        boolean tie = true;
        for(int i = 0 ; i < 6 ; i++){
            tie = (getFirstAvailableRow(i) == -1) && tie;
        }

        if(tie) return -1;

        return 0;
    }


    /**
     * Announces Winner of the game. Displaces message with winner.
     * @param b - game board.
     */
    private boolean announceWinner(int b[][]){
        int state = getWinner(b);
        if (state == 0) return false;
        gamedone = true;

        //resets moves played this game.
        String plays = "";
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(MyConstants.GameBoard.PLAYERBOARD, plays);

        //show restart button
        findViewById(R.id.restart).setVisibility(View.VISIBLE);
        TextView textView = (TextView) findViewById(R.id.state);
        textView.setVisibility(View.VISIBLE);

        //set the content of the label
        if(state == MyConstants.GameBoard.PLAYER){
            textView.setText(R.string.bluewon);
        }
        else if (state == MyConstants.GameBoard.DROID){
            textView.setText(R.string.redwon);
        }
        else{
            textView.setText(R.string.tie);
        }
        editor.apply();

        return true;
    }


    /**
     * Class listening to columns.
     */
    private class PlayListener implements View.OnClickListener{

        int col, colId;

        //take column's number to play in that column when invoked
        public PlayListener(int c){
            col = c;
            colId = getColId(c);
        }

        @Override
        public void onClick(View v){
            if(!gamedone) {
                //if the column is full do nothing
                if (getFirstAvailableRow(col) == -1){
                    return;
                }
                // sets boolean blueplaying indicating which player is playing currently.
                if (blueplaying) {
                    play(1, col, GameBoardPlayer.this.gameBoard);
                    blueplaying = false;
                } else {
                    play(2, col, GameBoardPlayer.this.gameBoard);
                    blueplaying = true;
                }

                if (announceWinner(GameBoardPlayer.this.gameBoard)){
                    return;
                }
            }

        }
    }


    /**
     * Class listening to restart button
     */
    private class RestartListener implements View.OnClickListener{
        @Override
        public void onClick(View v){

            //resets all the circles
            ViewGroup cols =(ViewGroup) findViewById(R.id.wrapper);
            for(int i = 0, k = 0; i < 6 ;k++){
                View child = cols.getChildAt(k);
                if (!(child instanceof ViewGroup)){continue;}
                ViewGroup col = (ViewGroup) child;
                for(int j = 0 ; j < col.getChildCount() ; j++){
                    col.getChildAt(j).setBackground(getResources().getDrawable(R.drawable.circle_white));
                }
                i++;
            }

            //resets the board-array
            for (int i = 0 ; i < 6 ; i++) {Arrays.fill(gameBoard[i], 0);}

            //hides it self and the state's textView
            findViewById(R.id.state).setVisibility(View.INVISIBLE);
            v.setVisibility(View.INVISIBLE);

            //first player's turn to play
            blueplaying = true;
            gamedone = false;
        }
    }


    /**
     * Method listens to Back button.
     */
    public void onBackClick(View view){
        switch(view.getId()){
            case R.id.goback:
                finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(player.isPlaying()){
            player.pause();
            playermoveplayer.pause();
            secondmoveplayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if(!player.isPlaying() && sp.getInt(MyConstants.GameOptions.MUSIC, 1) ==  1) {
            player.setLooping(true);
            player.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(player.isPlaying()){
            player.pause();
            playermoveplayer.pause();
            secondmoveplayer.pause();
        }
    }


}
